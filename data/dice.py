import random
import re

class rkat_dice:
#Sothdanie kubika
	def __init__(self):
		self.dice=20
		self.number=1
		self.typer=0
		self.mod=0
		self.dstr='d20'
		self.tf = 0
		self.reg = re.compile('\.dice (([0-9]{0,3})d(F|[0-9]{1,9})?((\+)|(-){0,1})([0-9]{0,4})(a{0,1})p{0,1}([0-9]{0,3}))')
#Prostaia dramatic 
	def roll_dice(self, dice):
		random.seed()
		x=random.randint(1, dice)
		y=random.randint(1, dice)
		if (x == 1 and self.typer == 2):
			x=random.randint(1, dice)
		elif (x>y and self.typer == 1):
			x=random.randint(1, dice)
		elif (x<y and self.typer == 0):
			x=random.randint(1, dice)
		if(self.tf == 1):
			x = x - 2
		return x
	
#Parsing     
	def parse(self, string):
		print string
		self.tf = 0
		p=self.reg.match(string)		
		try:
			self.dstr=p.group(1)
		except:
			self.dstr='d20'			
		try:
			self.number=1*int(p.group(2))
		except:
			self.number=1			
		try:
			self.dice=1*int(p.group(3))
		except:
			try:
				if(p.group(3) == 'F'):
					self.tf = 1
					self.dice = 3
			except:
				self.dice=20
		try:
			self.mod=1*int(p.group(7))
		except:
			self.mod=0
		try:
			if(p.group(8) == 'a'):
				self.general=1
			else:
				self.general=-1
		except:
			self.general=-1
		try:
			self.porog=1*int(p.group(9))
		except:
			self.porog=-1
		flak=''
		try:
			flak=p.group(4)
		except:
			flak='+'		
		if flak=='-':
			self.mod=-1*self.mod
	
#Dice-met
	def roller(self, x='d20'):
		x_n = 0
		self.parse(x)
		numbekr=0
		zb=self.number
		D = {}
		while zb>0:
			if(self.general == -1):
				buffy=self.roll_dice(self.dice)
			else:
				buffy=self.roll_dice(self.dice)+self.mod
				buffyold = buffy
				if(buffy < 0):
					buffy = 0

			if(zb == self.number):
				string=''
			else:
				if(self.tf == 1):
					string=string+", "
				else:
					string=string+"+"
			
			fate = {0:'0', 1:'+', -1:'-'}
			if(self.tf == 1):
				string=string+str(fate[buffy])
			elif(self.general == -1):
				string=string+str(buffy)
			else:
				string=string+str(buffy)+"["+str(buffyold-self.mod)+"]"


			if(D.has_key(buffy)):
				D[buffy] = D[buffy] + 1
			else:
				D[buffy] = 1

			numbekr=numbekr+buffy
			if(int(self.porog) <= buffy):
				x_n = x_n + 1

			zb=zb-1
		otvet = self.dstr+"="
		if(self.tf == 1):
			otvet = otvet + str(numbekr+self.mod)+"= "+string+", mod: "+str(self.mod)
		elif(self.general == -1):
			otvet = otvet + str(numbekr+self.mod)+"="+string+"+"+str(self.mod)
		else:
			otvet = otvet + str(numbekr)+"="+string
		if(self.porog > 0):
			otvet = otvet+"\n-->"+str(self.porog)+"-"+str(x_n)+"\n"
			for key, value in D.items():
				otvet = otvet +"["+str(key) + "=>" +str(value)+"], "
				
		return otvet
